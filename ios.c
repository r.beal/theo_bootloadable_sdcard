/* 
 * File:   ios.c
 * Author: rbeal
 *
 * Created on July 30, 2018, 1:41 PM
 */

#include <xc.h>
#include <string.h>
#include "ios.h"
#include "soft_timer.h"

u8 led_lat_reg[NB_LED];
u8 led_state[NB_LED];

u8 button_port_reg[NB_BUTTON];
u8 button_state[NB_BUTTON];

/*
 * Function: ios_led_init
 * ----------------------------
 *   no Return
 *   no parameters
 * 
 *   Init the led machine and configure LED's IOs ports as output
 *   
 */
void ios_led_init(void) {
    
    /*  
     * led_lat_reg content (8b):
     * # # # #    |    # # # #
     * bit of LATx      LATx
     *
     * 
     * 
     * LATA = 0
     * LATB = 1
     * LATC = 2
     */
    
    led_lat_reg[LED_1]  = 2 + (0 <<4);  // LED1_1 -> RC0
    led_lat_reg[LED_2]  = 2 + (1 <<4);  // LED1_2 -> RC1
    led_lat_reg[LED_3]  = 2 + (2 <<4);  // LED2_1 -> RC2
    led_lat_reg[LED_4]  = 2 + (3 <<4);  // LED2_2 -> RC3
    led_lat_reg[LED_5]  = 2 + (4 <<4);  // LED3_1 -> RC4
    led_lat_reg[LED_6]  = 2 + (5 <<4);  // LED3_2 -> RC5
    led_lat_reg[LED_7]  = 2 + (6 <<4);  // LED4_1 -> RC6
    led_lat_reg[LED_8]  = 2 + (7 <<4);  // LED4_2 -> RC7
    led_lat_reg[LED_9]  = 0 + (10 <<4); // LED5_1 -> RA10
    led_lat_reg[LED_10] = 0 + (2 <<4);  // LED5_2 -> RA2
    
    // Configure as output mode
    TRISCbits.TRISC0 = 0;
    TRISCbits.TRISC1 = 0;
    TRISCbits.TRISC2 = 0;
    TRISCbits.TRISC3 = 0;
    TRISCbits.TRISC4 = 0;
    TRISCbits.TRISC5 = 0;
    TRISCbits.TRISC6 = 0;
    TRISCbits.TRISC7 = 0;
    TRISAbits.TRISA10 = 0;  // A10 as output
    TRISAbits.TRISA2 = 0;   // A2  as output
    
    memset(led_state, LED_OFF, NB_LED);
    soft_timer_set_period(eTIMER_LED, LED_PERIOD); 
}

/*
 * Function: ios_led_machine
 * ----------------------------
 *   no Return
 *   no parameters
 *   
 *   This function must be executed in main loop
 * 
 *   The led machine read led_state register
 *   and drive physical led output
 *   
 *   It read eTIMER_LED soft timer to use blink mode
 *   
 */
void ios_led_machine(void) {
    
    u8 i;
    u8 blink_toggle = 0;
    
    if (soft_timer_getState(eTIMER_LED) != eTIMER_RUNNING) {
        soft_timer_start(eTIMER_LED);
        blink_toggle = 1;
    }
    
    for (i=0; i<NB_LED; i++) {
        
        switch (led_state[i]) {
            
            case LED_OFF:
                ios_change_output(led_lat_reg[i], 0);
                break;
                
            case LED_ON:
                ios_change_output(led_lat_reg[i], 1);
                break;
                
            case LED_BLINK:
                led_state[i] = LED_BLINK_ON;
                break;
                
            case LED_BLINK_ON:
                ios_change_output(led_lat_reg[i], 1);
                led_state[i] = LED_BLINK_WAIT_OFF;
                break;
                
            case LED_BLINK_WAIT_OFF:
                if (blink_toggle)
                    led_state[i] = LED_BLINK_OFF;
                break;
                
            case LED_BLINK_OFF:
                ios_change_output(led_lat_reg[i], 0);
                led_state[i] = LED_BLINK_WAIT_ON;
                break;
                
            case LED_BLINK_WAIT_ON:
                if (blink_toggle)
                    led_state[i] = LED_BLINK_ON;
                break;
        }
    }
}

/*
 * Function: ios_led_setState
 * ----------------------------
 *   no Return
 *   led: type eLED, led to change state
 *   state: type eLedState, new state
 *   
 *   User can call this function to change the led state
 *   
 */
void ios_led_setState(eLed led, eLedState state) {
    
    // If the led is already in blink mode and the new state is blink...
    if ((led_state[led] == LED_BLINK
     || led_state[led] == LED_BLINK_OFF
     || led_state[led] == LED_BLINK_ON
     || led_state[led] == LED_BLINK_WAIT_OFF
     || led_state[led] == LED_BLINK_WAIT_ON)
     && state == LED_BLINK)
        return ;
    
    led_state[led] = state; // set new state
}

/*
 * Function: ios_change_output
 * ----------------------------
 *   no Return
 *   lat: physical port
 *   val: value (1 || 0)
 *   
 *   User do not call this function
 *   
 */
void ios_change_output(u8 lat, u8 val) {
    
    switch (lat & 0x0F) {
        case 0:
            if (val) {
                LATA |= 1 <<((lat>>4) &0x0F);
                /*
                 * OLD_LATA = 0b0000000100000100
                 * REQ      = 0b0000000000100000
                 * NEW_LATA = 0b0000000100100100
                 * 
                 */
            }
            else {
                LATA &= ~(1 <<((lat>>4) &0x0F));
                /*
                 * OLD_LATA = 0b0000000100000100
                 * REQ      = 0b0000000000100000
                 * NEW_LATA = 0b0000000100000100
                 * 
                 */
            }
            break;
            
        case 1:
            if (val) {
                LATB |= 1 <<((lat>>4) &0x0F);
            }
            else {
                LATB &= ~(1 <<((lat>>4) &0x0F));
            }
            break;
            
        case 2:
            if (val) {
                LATC |=   1 <<((lat>>4) &0x0F);
            }
            else {
                LATC &= ~(1 <<((lat>>4) &0x0F));
            }
            break;
    }
}

/*
 * Function: ios_button_init
 * ----------------------------
 *   no Return
 *   no Parameters
 *   
 *   Init the button machine and configure BUTTON's IOs ports as input
 *   
 */
void ios_button_init(void) {
    /*  
     * button_port_reg content (8b):
     * # # # #    |    # # # #
     * bit of PORTx      PORTx
     */
    
    /*
     * PORTA = 0
     * PORTB = 1
     * PORTC = 2
     */
    
    button_port_reg[BUTTON_1]  = 1 + (2 <<4);  // BUTTON_1 -> RB2 (bootloader)
    button_port_reg[BUTTON_2]  = 1 + (3 <<4);  // BUTTON_2 -> RB3
    button_port_reg[BUTTON_4]  = 1 + (4 <<4);  // BUTTON_3 -> RB4
    button_port_reg[BUTTON_3]  = 1 + (7 <<4);  // BUTTON_4 -> RB7
    
    // Input mode
    TRISBbits.TRISB2 = 1; // RB2 as input
    TRISBbits.TRISB3 = 1; // RB3 as input
    TRISBbits.TRISB4 = 1; // RB4 as input
    TRISBbits.TRISB7 = 1; // RB7 as input
    
    memset(button_state, BUTTON_RELEASED, NB_BUTTON); // reset all states
}

void ios_button_machine(void) {
    
    u8 i;
    
    for (i=0; i<NB_BUTTON; i++)
        button_state[i] = ios_read_input(button_port_reg[i]);
}

eButtonState ios_button_getState(eButton btn) {
    
    return button_state[btn];
}

u8 ios_read_input(u8 port) {
    
    u8 ret;
    
    switch (port & 0x0F) {
        
        case 0:
                ret = PORTA & (1 <<((port>>4) &0x0F));
            break;
        case 1:
                ret = PORTB & (1 <<((port>>4) &0x0F));
            break;
        case 2:
                ret = PORTC & (1 <<((port>>4) &0x0F));
            break;
    }
    
    return (ret > 0);
    
}