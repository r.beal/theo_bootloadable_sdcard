/* 
 * File:   init.c
 * Author: rbeal
 *
 * Created on August 1, 2018, 01:04 AM
 */

#include <xc.h>
#include "defines.h"
#include "fileio.h"
#include "drv_spi.h"
#include "rtcc.h"
#include "sd_spi.h"
#include "init.h"

BSP_RTCC_DATETIME dateTime;

// The gSDDrive structure allows the user to specify which set of driver functions should be used by the
// FILEIO library to interface to the drive.
// This structure must be maintained as long as the user wishes to access the specified drive.


// Declare a state machine for our device


// Some sample data to write to the file



void init_sd_fileio(void) {

    dateTime.bcdFormat = false;
    RTCC_BuildTimeGet(&dateTime);
    RTCC_Initialize (&dateTime);

    // Initialize the library
    if (!FILEIO_Initialize())
    {
        while(1);
    }

    // Register the GetTimestamp function as the timestamp source for the library.
    FILEIO_RegisterTimestampGet (GetTimestamp);
    
}

void GetTimestamp (FILEIO_TIMESTAMP * timeStamp)
{
    BSP_RTCC_DATETIME dateTime;

    dateTime.bcdFormat = false;

    RTCC_TimeGet(&dateTime);

    timeStamp->timeMs = 0;
    timeStamp->time.bitfield.hours = dateTime.hour;
    timeStamp->time.bitfield.minutes = dateTime.minute;
    timeStamp->time.bitfield.secondsDiv2 = dateTime.second / 2;

    timeStamp->date.bitfield.day = dateTime.day;
    timeStamp->date.bitfield.month = dateTime.month;
    // Years in the RTCC module go from 2000 to 2099.  Years in the FAT file system go from 1980-2108.
    timeStamp->date.bitfield.year = dateTime.year + 20;;
}

