/* 
 * File:   init.h
 * Author: rbeal
 *
 * Created on August 1, 2018, 1:04 AM
 */

#ifndef INIT_H
#define	INIT_H

#include "defines.h"
#include "fileio.h"
#include "drv_spi.h"
#include "rtcc.h"
#include "sd_spi.h"

void init_sd_fileio(void);
void GetTimestamp (FILEIO_TIMESTAMP * timeStamp);

typedef enum
{
    DEMO_STATE_NO_MEDIA = 0,
    DEMO_STATE_MEDIA_DETECTED,
    DEMO_STATE_DRIVE_MOUNTED,
    DEMO_STATE_DONE,
    DEMO_STATE_FAILED
} DEMO_STATE;



#endif	/* INIT_H */

