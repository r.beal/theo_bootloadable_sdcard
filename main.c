/*
 * File:   main.c
 * Author: rbeal
 *
 * Created on December 30, 2017, 9:30 PM
 */

#include <xc.h>
#include "types.h"
#include "ios.h"
#include "soft_timer.h"
#include "timer.h"
#include "sdcard/defines.h"

#include "sdcard/fileio.h"
#include "sdcard/drv_spi.h"
#include "sdcard/rtcc.h"
#include "sdcard/sd_spi.h"
#include "sdcard/init.h"

void ioInit(void);

extern FILEIO_SD_DRIVE_CONFIG sdCardMediaParameters;
DEMO_STATE demoState = DEMO_STATE_NO_MEDIA;
FILEIO_OBJECT file;

u8 sampleData[9] = "boot !\r\n";

const FILEIO_DRIVE_CONFIG gSdDrive =
{
    (FILEIO_DRIVER_IOInitialize)FILEIO_SD_IOInitialize,                      // Function to initialize the I/O pins used by the driver.
    (FILEIO_DRIVER_MediaDetect)FILEIO_SD_MediaDetect,                       // Function to detect that the media is inserted.
    (FILEIO_DRIVER_MediaInitialize)FILEIO_SD_MediaInitialize,               // Function to initialize the media.
    (FILEIO_DRIVER_MediaDeinitialize)FILEIO_SD_MediaDeinitialize,           // Function to de-initialize the media.
    (FILEIO_DRIVER_SectorRead)FILEIO_SD_SectorRead,                         // Function to read a sector from the media.
    (FILEIO_DRIVER_SectorWrite)FILEIO_SD_SectorWrite,                       // Function to write a sector to the media.
    (FILEIO_DRIVER_WriteProtectStateGet)FILEIO_SD_WriteProtectStateGet,     // Function to determine if the media is write-protected.
};

int main(void) {
    
    u8 error_sdcard = 0;
    
    ioInit();
    timer_1_init();
    soft_timer_init();
    
    ios_led_init();
    ios_button_init();
    
    init_sd_fileio();
    
    if (PORTAbits.RA9 == 0) {
        
        if (FILEIO_MediaDetect(&gSdDrive, &sdCardMediaParameters) != 1)
            error_sdcard++;

        if ((FILEIO_DriveMount('A', &gSdDrive, &sdCardMediaParameters) != FILEIO_ERROR_NONE) && !error_sdcard)
            error_sdcard++;


        if ((FILEIO_Open(&file, "BOOT.TXT", FILEIO_OPEN_WRITE | FILEIO_OPEN_APPEND | FILEIO_OPEN_CREATE) != FILEIO_RESULT_FAILURE) && !error_sdcard)
            if (FILEIO_Write (sampleData, 1, 8, &file) == 8)
                ios_led_setState(LED_1, LED_BLINK);
            else
                error_sdcard++;
        else
            error_sdcard++;

        if ((FILEIO_Close (&file) != FILEIO_RESULT_SUCCESS) && !error_sdcard)
            error_sdcard++;


        if ((FILEIO_DriveUnmount ('A') != FILEIO_RESULT_SUCCESS) && !error_sdcard)
            error_sdcard++;

        if (error_sdcard)
            ios_led_setState(LED_2, LED_BLINK);
    
    }
    for (;;) {
        
        ios_led_machine();
        ios_button_machine();
        
        if (ios_button_getState(BUTTON_4) == BUTTON_PRESSED)
            asm("reset"); // For freestyle reset button
        
        if (PORTAbits.RA9 == 0)
            ios_led_setState(LED_9, LED_ON);
        else
            ios_led_setState(LED_9, LED_OFF);
        
    }
    
    return 1;
}


void ioInit(void) {
    
    // Disable alls Analog input
    ANSA = 0;
    ANSB = 0;
    ANSC = 0;
    
    TRISCbits.TRISC8 = 0; // CS spi : output mode
    TRISAbits.TRISA9 = 1; // sdcard Presence
    
    OSCCONbits.IOLOCK = 0; // unlock PPS
    RPOR7bits.RP14R = _RPOUT_SDO1;
    RPOR7bits.RP15R = _RPOUT_SCK1OUT;
    RPINR20bits.SDI1R = 25;
    OSCCONbits.IOLOCK = 1; // lock PPS
    
}

inline void USER_SdSpiSetCs(uint8_t a)
{
    LATCbits.LATC8 = a;
}

inline bool USER_SdSpiGetCd(void)
{
    TRISCbits.TRISC5 = 0;
    
    if (PORTAbits.RA9)
        return false;
    else
        return true;
}

inline bool USER_SdSpiGetWp(void)
{
    return false;
}

void USER_SdSpiConfigurePins(void)
{
    
    RPOR7bits.RP14R = _RPOUT_SDO1;
    RPOR7bits.RP15R = _RPOUT_SCK1OUT;
    RPINR20bits.SDI1R = 25;
    
    // Deassert the chip select pin
    LATCbits.LATC8 = 1;
    // Configure CS pin as an output
    TRISCbits.TRISC8 = 0;
    // Configure CD pin as an input
    TRISAbits.TRISA9 = 1;
    // Configure WP pin as an input
    //TRISFbits.TRISF1 = 1;
}

FILEIO_SD_DRIVE_CONFIG sdCardMediaParameters =
{
    1,                                  // Use SPI module 2
    USER_SdSpiSetCs,                    // User-specified function to set/clear the Chip Select pin.
    USER_SdSpiGetCd,                    // User-specified function to get the status of the Card Detect pin.
    USER_SdSpiGetWp,                    // User-specified function to get the status of the Write Protect pin.
    USER_SdSpiConfigurePins             // User-specified function to configure the pins' TRIS bits.
};