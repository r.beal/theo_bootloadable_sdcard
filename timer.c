/* 
 * File:   timer.c
 * Author: rbeal
 *
 * Created on July 30, 2018, 2:46 PM
 */
#include <xc.h>
#include "types.h"
#include "timer.h"
#include "soft_timer.h"


void timer_1_init(void) {
    
    // Input -> Fosc/2 = 32MHz/2 = 16MHz
    // Prescaler -> 1 | out = 16MHz/1 = 0.000000063 = 63nS
    
    // To s16errupt every 1mS :
    // 0.001 / 0.000000063 = 15873
    // The counter will count from TMR1's register to PR1's register
    
    TMR1 = 0; // this counter will HARD auto-inc
    PR1 = 15873;
    T1CONbits.TCS = 0; // Use Fosc/2 as input
    T1CONbits.TCKPS = 0b00; // Prescaler = 1
    T1CONbits.TON = 1; // Start timer block
    
    IFS0bits.T1IF = 0; // Validate IT
    IEC0bits.T1IE = 1; // Enabkle IT
}


void __attribute__((interrupt,no_auto_psv)) _T1Interrupt( void ) {
    TMR1 = 0;
    IFS0bits.T1IF = 0; // to validate IT
    soft_timer_machine(); // call every 1mS
}
